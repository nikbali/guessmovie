package com.example.android.guessmovie;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.TmdbMovies;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import info.movito.themoviedbapi.tools.MovieDbException;

public class process_game extends FragmentActivity {

    ListView listView_films;
    private ImageFragment image_fragment = new ImageFragment();
    private ButtonsFragment buttons_fragment = new ButtonsFragment();
    private ArrayList<MovieDb> films_load ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_game);
        listView_films = (ListView) findViewById(R.id.listview_films);
        films_load = new ArrayList<MovieDb>();

        ArrayAdapter<String> adpter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, InitialData());
        listView_films.setAdapter(adpter);


        listView_films.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView drow = (ImageView) findViewById(R.id.imageView);
                drow.setImageResource(R.drawable.tor);

            }
        });
    }




    private List<String> InitialData(){
        List<String> data = new ArrayList<>();
         FilmFromAPI mt = new FilmFromAPI();
        mt.execute();
        try{
        films_load   = mt.get();
        data.add(films_load.get(1).getTitle());
        data.add(films_load.get(2).getTitle());
        data.add(films_load.get(3).getTitle());
        data.add(films_load.get(4).getTitle());
        return data;
    } catch (InterruptedException e) {
        e.printStackTrace();
            return null;
    } catch (ExecutionException e) {
        e.printStackTrace();
            return null;
    }}


    private class FilmFromAPI extends AsyncTask<String , Void  , ArrayList<MovieDb>> {

        protected ArrayList<MovieDb> doInBackground(String... urls) {
            try {
                TmdbMovies movies = new TmdbApi("90c2cf6392b3f74022474d5a5e385061").getMovies();
                ArrayList<MovieDb> films = new ArrayList<MovieDb>();
                films.addAll(movies.getPopularMovies("ru", 1).getResults());
                return films;
            } catch (MovieDbException ex) {
                throw ex;
            }
        }

        protected void onPostExecute(ArrayList<MovieDb> result) {
            films_load.addAll(result);
        }
    }

}
